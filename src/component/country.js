import React, { Component } from "react";
import * as CountryApi from "./fetchingApi";
import ClockLoader from "react-spinners/ClockLoader";
import { Link } from "react-router-dom";

class CountryCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countries: [],
      isLoading: true,
      hasError: false,
      searchTerm: [],
      regionSelect: "All",
      regions: [],
    };
  }
  componentDidMount() {
    CountryApi.getCountries()
      .then((res) => {
        this.setState({
          countries: res,
          isLoading: false,
          hasError: false,
        });
      })
      .catch((err) => {
        this.setState({
          err,
          isLoading: false,
          hasError: true,
        });
      });
  }

  render() {
    const filteredCountries = this.state.countries.filter(
      (c) =>
        c.name.common.toLowerCase().includes(this.state.searchTerm) &&
        (this.state.regionSelect === "All" ||
          c.region === this.state.regionSelect)
    );
    if (this.state.isLoading) {
      return (
        <div className="spinner">
          <ClockLoader color="#36d7b7" />
        </div>
      );
    }
    if (this.state.hasError) {
      return (
        <div className="error">
          <h1 color="#36d7b7">Internal Server Error</h1>
        </div>
      );
    }
    const regions = [
      ...this.state.countries.reduce((acc, curr) => {
        acc.add(curr.region);
        return acc;
      }, new Set()),
    ];
    return (
      <>
        <div className="header">
          <h1>Where in the world?</h1>
        </div>
        <div className="container">
          <div className="filter">
            <input
              className="search"
              placeholder="Search"
              type="text"
              value={this.state.searchTerm}
              onChange={(e) => this.setState({ searchTerm: e.target.value })}
            />

            <select
              className="select"
              placeholder="Type"
              value={this.state.regionSelect}
              onChange={(e) => this.setState({ regionSelect: e.target.value })}
            >
              <option value="All">All</option>
              {regions.map((el) => (
                <option key={el}>{el}</option>
              ))}
            </select>
          </div>
          <div className="row">
            {filteredCountries.length === 0 && <div>not found </div>}
            {filteredCountries.map((item) => {
              return (
                <div key={item.cca2} className="column">
                  <div className="card">
                    <img src={item.flags.png} alt="flags" width="220px"></img>
                    <Link to={"/code/" + item.cca2}>
                      <h3>{item.name.common}</h3>
                    </Link>
                    <p>Region: {item.region}</p>
                    <p>Population: {item.population}</p>
                    <p>Capital: {item.capital}</p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default CountryCard;
