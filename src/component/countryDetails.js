import React, { Component } from "react";
import * as CountryApi from "./fetchingApi";
import ClockLoader from "react-spinners/ClockLoader";
import { Link } from "react-router-dom";

class CountryDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: [],
      isLoading: true,
      hasError: false,
    };
  }
  componentDidMount() {
    const { code } = this.props.match.params;

    console.log("calling Api");
    CountryApi.getCountryByName(code)
      .then((res) => {
        this.setState({
          country: res[0],
          isLoading: false,
          hasError: false,
        });
      })
      .catch((err) => {
        this.setState({
          country: null,
          isLoading: false,
          hasError: true,
        });
      });
  }
  async componentDidUpdate(prevProps) {
    if (prevProps.match.params.code !== this.props.match.params.code) {
      try {
        const { code } = this.props.match.params;
        const data = await CountryApi.getCountryByName(code);
        this.setState({
          country: data[0],
          isLoading: false,
          hasError: false,
        });
      } catch (err) {
        this.setState({
          countryy: null,
          isLoading: false,
          hasError: true,
        });
      }
    }
  }

  render() {
    console.log(this.state.country);
    if (this.state.isLoading) {
      return (
        <div className="spinner">
          <ClockLoader color="#36d7b7" />
        </div>
      );
    }
    if (this.state.hasError) {
      return (
        <div className="error">
          <h1 color="#36d7b7">Internal Server Error</h1>
        </div>
      );
    }
    return (
      <>
        <div className="header">
          <h1>Where in the world?</h1>
        </div>
        <div className="country-details-container">
          <div className="button">
            <Link to="/">Back</Link>
          </div>
          <br />
          <div className="details-container">
            <div className="flag">
              <img src={this.state.country.flags.png} alt="country" />
            </div>
            <div className="country-details">
              <h2>{this.state.country.name.common}</h2>
              <div className="sub-details">
                <div>
                  <p>
                    Native Name:
                    {
                      Object.values(this.state.country.name.nativeName)[0]
                        .common
                    }
                  </p>

                  <p>Population:{this.state.country.population}</p>
                  <p>Region:{this.state.country.region}</p>
                  <p>Sub Region:{this.state.country.subregion}</p>
                  <p>Capital:{this.state.country.capital}</p>

                  <p>Top Level Domain:{this.state.country.tld}</p>
                  <p>
                    Currencies:{" "}
                    {Object.values(this.state.country.currencies)[0].name}
                  </p>
                  <p>Language:{Object.values(this.state.country.languages)}</p>

                  {this.state.country.borders &&
                  this.state.country.borders.length ? (
                    <p className="last-child">
                      Border Countries:
                      {this.state.country.borders.map((el) => {
                        return (
                          <Link key={el} to={`/code/${el}`}>
                            <button>{el}</button>
                          </Link>
                        );
                      })}
                    </p>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default CountryDetails;
